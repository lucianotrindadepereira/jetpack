﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour {

	public string playerTag;
	public AudioClip audioCollected;
	public int points = 100;

	LevelManager levelManager;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		// get points
		if (playerTag == other.tag) {
			// add score
			levelManager.UpdateScore(points);
			//play audio
			AudioSource.PlayClipAtPoint(audioCollected, transform.position);
			// self-destroing
			Destroy (this.gameObject);
		}
	}

}
