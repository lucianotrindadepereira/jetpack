﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

	public AudioClip audioTeleport;
	public LayerMask charactersLayer;

	GameObject[] teleports;
	float posX = -1000;
	float posY = -1000;
	int secondsTeleport = 3;
	static float timeTeleport = 0;

	// Use this for initialization
	void Start () {
		teleports = GameObject.FindGameObjectsWithTag (tag);
		foreach (GameObject teleport in teleports)
		{
			if (teleport != gameObject) {
				posX = teleport.transform.position.x;
				posY = teleport.transform.position.y;

				Debug.Log (posX + " " + posY + " " + gameObject.name);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		// teletransport
		//Debug.Log ("Layers " + other.gameObject.layer + " "  + charactersLayer.value);
		if (((1 << other.gameObject.layer) & charactersLayer) != 0) {
			if (posX > -1000 && Time.fixedTime > timeTeleport) {
				// play audio
				AudioSource.PlayClipAtPoint (audioTeleport, transform.position);
				// move
				other.gameObject.transform.position = new Vector3 (posX, posY, 1);

				// waiting before next teleport
				timeTeleport = Time.fixedTime + secondsTeleport;
			}
		}
	}

}
