﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderCollider : MonoBehaviour {

	public LayerMask ladderLayer;
	public GameObject Player;

	int ladder = 0;
	int flag = 1;
	int flagRandom = 1;
	int flagPlayer = 1;

	public void setLadder(int value) {
		ladder = ladder + value;

		// set upd or down in ladder
		if (ladder <= 0) {
			// machine state mode
			flag = flag * -1;

			// random mode
			flagRandom = Random.Range (-1, 1);
			if (flagRandom == 0)
				flagRandom = 1;

			// playr follow mode
			if (Player != null) {
				//Debug.Log (Mathf.RoundToInt (transform.position.y) + " " + Mathf.RoundToInt (Player.transform.position.y));
				if (Mathf.RoundToInt (transform.position.y) > Mathf.RoundToInt (Player.transform.position.y))
					flagPlayer = -1;
				else if (Mathf.RoundToInt (transform.position.y) < Mathf.RoundToInt (Player.transform.position.y))
					flagPlayer = 1;
				else
					flagPlayer = 0;
			}
		}
	}

	public bool getLadder() {
		if (ladder > 0)
			return true;
		else
			return false;
	}

	public int getFlag() {
		return flag;
	}

	public int getFlagRandom() {
		return flagRandom;
	}

	public int getFlagPlayer() {
		return flagPlayer;
	}

	public void setFlagPlayer(int value) {
		flagPlayer = value;
	}
}
