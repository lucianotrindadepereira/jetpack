﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freeze : MonoBehaviour {

	public string playerTag;
	public AudioClip audioCollected;

	LevelManager levelManager;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		// get points
		if (playerTag == other.tag) {
			// coin collected
			levelManager.FreezeSet();
			//play audio
			AudioSource.PlayClipAtPoint(audioCollected, transform.position);
			// self-destroing
			Destroy (this.gameObject);
		}
	}

}
