﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
	public int coinsNeeded = 10;
	public int startLives = 3;
	public int lengthLives = 2;
	public Text textLives;
	public int lengthScore = 5;
	public Text textScore;
	public GameObject Fuel;
	public int secondsRespawn = 3;
	public GameObject GameOver;
	public GameObject MobileControl;
	public int secondsNextLevel = 3;
	public int secondsFreezing = 5;
	public Color colorFreezing;
	public Camera mainCamera;

	Door doorLevel;
	static int lives = 0;
	//static int level = 0;
	static long score = 0;
	static float fuel = 0;
	float timeRespawn = 0;
	float timeFreezing = 0;
	bool freeze = false;

	string stringZeros = "000000000000000";

	// Use this for initialization
	void Start () {
		doorLevel = GameObject.FindObjectOfType<Door>();
		if (lives == 0)
			lives = startLives;
		UpdateLives (0);
		UpdateScore (0);
		fuel = 0;
		UpdateFuel (0);
		freeze = false;
	}
	
	// Update is called once per frame
	void Update () {
		// exit game
		if (Input.GetKey(KeyCode.Escape)) {
			Application.Unload ();
			//Application.Quit ();
		}
		// verify respawn
		if (timeRespawn > 0) {
			if (Time.fixedTime > timeRespawn + secondsRespawn) {
				timeRespawn = 0;
				// verify game-over
				if (lives <= 0) {
					GameOver.SetActive (true);
					MobileControl.SetActive (false);
				} else {
					// re-load the current scene
					SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
				}
			}
		}
		// verify freezing
		if (timeFreezing > 0) {
			if (Time.fixedTime > timeFreezing + secondsFreezing) {
				timeFreezing = 0;
				freeze = false;
				mainCamera.backgroundColor = Color.black;
			} else if (Time.fixedTime > (timeRespawn + secondsFreezing - 1)) {
				mainCamera.backgroundColor = Color.Lerp (colorFreezing, Color.black, 0.8f);
			} else if (Time.fixedTime > (timeRespawn + secondsFreezing - 2)) {
				mainCamera.backgroundColor = Color.Lerp (colorFreezing, Color.black, 0.6f);
			} else if (Time.fixedTime > (timeRespawn + secondsFreezing - 3)) {
				mainCamera.backgroundColor = Color.Lerp (colorFreezing, Color.black, 0.3f);
			}
		}

		// open the door
		if (coinsNeeded <= 0) {
			if (doorLevel != null)
				doorLevel.OpenDoor ();
		}
	}

	public void UpdateLives (int value) {
		string stringTemp;
		lives = lives + value;
		stringTemp = stringZeros + lives.ToString();
		textLives.text = stringTemp.Substring (stringTemp.Length-lengthLives , lengthLives);
	}

	public void UpdateScore (long value) {
		string stringTemp;
		score = score + value;
		stringTemp = stringZeros + score.ToString();
		textScore.text = stringTemp.Substring (stringTemp.Length-lengthScore , lengthScore);
	}

	public void UpdateFuel (float value) {
		fuel = fuel + value;
		if (fuel > 1)
			fuel = 1;
		else if (fuel < 0)
			fuel = 0;
		Fuel.transform.localScale = new Vector3 (fuel, 1, 1);
	}

	public float GetFuel () {
		return fuel;
	}

	public void RestartLevel () {
		//waiting before respawn
		timeRespawn = Time.fixedTime;
	}

	public void NextLevel () {
		//next level
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void CoinCollected () {
		coinsNeeded = coinsNeeded - 1;
	}

	public bool Freeze () {
		return freeze;
	}

	public void FreezeSet () {
		freeze = true;
		mainCamera.backgroundColor = colorFreezing;
		// waiting time freezing
		timeFreezing = Time.fixedTime;
	}
}
