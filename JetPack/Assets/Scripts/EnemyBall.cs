﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBall : MonoBehaviour {
	public int speed;
	public LayerMask obstacleLayer;
	public int artefactsLayer;
	public int ladderLayer;

	Rigidbody2D myBody;
	LevelManager levelManager;
	int grounded = 0;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
		myBody = this.GetComponent<Rigidbody2D> ();

		Physics2D.IgnoreLayerCollision (gameObject.layer, artefactsLayer);
		Physics2D.IgnoreLayerCollision (gameObject.layer, ladderLayer);
		Physics2D.IgnoreLayerCollision (gameObject.layer, gameObject.layer);
	}

	// Update is called once per frame
	void FixedUpdate () {
		//Debug.Log (grounded);

		//movement
		if (levelManager.Freeze()) {
			myBody.velocity = new Vector2 (0, 0);
		} else {
			if (grounded > 0)
				myBody.velocity = new Vector2 (speed * transform.localScale.x, myBody.velocity.y);
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			grounded++;
	}	

	void OnTriggerExit2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			grounded--;
	}
}
