﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStar : MonoBehaviour {
	public float speed;
	public GameObject Player;

	Rigidbody2D myBody;
	LevelManager levelManager;
	float directionX, directionY;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
		myBody = this.GetComponent<Rigidbody2D> ();

		Physics2D.IgnoreLayerCollision (gameObject.layer, gameObject.layer);
	}
	
	// Update is called once per frame
	void Update () {
		//movement
		if (levelManager.Freeze()) {
			myBody.velocity = new Vector2 (0, 0);
		} else if (Player != null) {
			// find player direction
			if (transform.position.x > Player.transform.position.x)
				directionX = -1;
			else
				directionX = 1;
			if (transform.position.y > Player.transform.position.y)
				directionY = -1;
			else
				directionY = 1;
			
			myBody.velocity = new Vector2 (directionX * speed, directionY * speed);
		}
	}
}
