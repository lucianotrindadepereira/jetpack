﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate1 : MonoBehaviour {

	public string attackTag;
	public GameObject explosion;
	public int points = 10;

	LevelManager levelManager;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D (Collision2D col) {
		// dying
		if (attackTag == col.collider.tag) {
			// add score
			levelManager.UpdateScore(points);
			// active the effect
			Instantiate(explosion, this.transform.position, Quaternion.identity);
			// self-destroing
			Destroy (this.gameObject);
		}
	}
}
