﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuel : MonoBehaviour {

	public string player_tag;
	public float fuelPercent;

	LevelManager levelManager;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other) {
		// dying
		if (player_tag == other.tag) {
			// add fuel
			levelManager.UpdateFuel (fuelPercent/100);
			// self-destroing
			Destroy (this.gameObject);
		}
	}
}
