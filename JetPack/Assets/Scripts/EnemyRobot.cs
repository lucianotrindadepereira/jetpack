﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRobot : MonoBehaviour {
	public int speed;
	public LayerMask obstacleLayer;
	public LayerMask ladderLayer;
	public int artefactsLayer;
	public int enemyLayer;
	public GameObject Player;

	Rigidbody2D myBody;
	LevelManager levelManager;
	LadderCollider ladderCollider;
	UpCollider upCollider;
	int grounded = 0;
	bool oldLadder = false;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		myBody = this.GetComponent<Rigidbody2D> ();
		ladderCollider = this.GetComponentInChildren<LadderCollider>();
		upCollider = this.GetComponentInChildren<UpCollider>();

		Physics2D.IgnoreLayerCollision (gameObject.layer, artefactsLayer);
		Physics2D.IgnoreLayerCollision (gameObject.layer, gameObject.layer);
		Physics2D.IgnoreLayerCollision (gameObject.layer, enemyLayer);
	}

	// Update is called once per frame
	void FixedUpdate () {
		// ignore gravity on ladder
		//Debug.Log (grounded + " " + ladderCollider.getLadder());

		// movement
		if (levelManager.Freeze()) {
			myBody.velocity = new Vector2 (0, 0);
		} else {
			if (ladderCollider.getLadder ()) {
				// climb ladder
				//if (upCollider.getCollider ())
				//	ladderCollider.setFlagPlayer (ladderCollider.getFlagPlayer() * -1);
				
				myBody.velocity = new Vector2 (0, Mathf.Abs (speed) * ladderCollider.getFlagPlayer () * upCollider.getFlag ());// up, down or waiting
			} else {
				upCollider.resetFlag ();
			}
			// move on the ground
			if (grounded > 0) {
				myBody.velocity = new Vector2 (speed * transform.localScale.x, myBody.velocity.y);
			} else if (oldLadder != ladderCollider.getLadder () && Player != null) {
				oldLadder = ladderCollider.getLadder ();
				// find player direction and flip
				if (Mathf.RoundToInt (transform.position.x) > Mathf.RoundToInt (Player.transform.position.x))
					transform.localScale = new Vector3 (1, transform.localScale.y, transform.localScale.z);
				else
					transform.localScale = new Vector3 (-1, transform.localScale.y, transform.localScale.z);
				myBody.velocity = new Vector2 (speed * transform.localScale.x, myBody.velocity.y);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			grounded ++;
	}

	void OnTriggerExit2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			grounded --;
	}
}
