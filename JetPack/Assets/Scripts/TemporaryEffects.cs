﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporaryEffects : MonoBehaviour {

	public float waitSeconds;

	// Use this for initialization
	void Start () {
		//start auto-destroy
		StartCoroutine(WaitingEffect());
	}

	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator WaitingEffect() {
		yield return new WaitForSeconds (waitSeconds);
		Destroy (this.gameObject);
	}
}
