﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpCollider : MonoBehaviour {

	public LayerMask obstacleLayer;

	int col = 0;
	int flag = 1;
	bool flagged = false;

	void Start() {
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0) {
			col++;

			// machine state mode
			if (!flagged) {
				flagged = true;
				flag = flag * -1;
			}
		}
	}
	void OnTriggerExit2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0) {
			col--;

			if (col <= 0)
				flagged = false;
		}
	}

	public bool getCollider() {
		if (col > 0)
			return true;
		else
			return false;
	}

	public int getFlag() {
		return flag;
	}

	public void resetFlag() {
		flag = 1;
	}
}
