﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

//Ref: https://www.youtube.com/watch?v=i363lrvWqcY
//Ref: https://www.youtube.com/watch?v=ohPcwvKJjPQ	
//Ref: https://www.youtube.com/watch?v=nGYObojmkO4&t=770s

public class Controller2D : MonoBehaviour {

	public float speed = 4.0f;
	public float jump = 4.0f;
	public float fly = 4.0f;
	public string enemyTag;
	public GameObject explosion;
	public LayerMask ladderLayer;
	public LayerMask obstacleLayer;

	LevelManager levelManager;
	Rigidbody2D myBody;
	ParticleSystem myXRay;

	int grounded = 0;
	int ladder = 0;
	float fuel = 0;
	float device_y = 0;
	float device_x = 0;
	int frontalCollision = 0;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
		myBody = this.GetComponent<Rigidbody2D> ();
		myXRay = this.GetComponentInChildren<ParticleSystem> ();
		// disable any effects
		myXRay.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (grounded + " " + ladder);

		// get controller
		device_x = CrossPlatformInputManager.GetAxis ("Horizontal");
		//device_y = CrossPlatformInputManager.GetAxis ("Vertical");

		//flip
		if (device_x > 0) {
			transform.localScale = new Vector3 (1, transform.localScale.y, transform.localScale.z);
		} else if (device_x < 0) {
			transform.localScale = new Vector3 (-1, transform.localScale.y, transform.localScale.z);
		}

		// x-ray
		if (CrossPlatformInputManager.GetButton ("Ray")) {
			// active the effect
			myXRay.gameObject.SetActive (true);
		} else {
			myXRay.gameObject.SetActive (false);
		}

		// movement
		/*if (ladder > 0 && (device_x != 0 || device_y != 0))
			myBody.velocity = new Vector2 (device_x * speed, device_y * speed / 2);
		else*/
		if (device_x != 0)
			myBody.velocity = new Vector2 (device_x * speed, myBody.velocity.y);

		//jump or fly
		fuel = levelManager.GetFuel();
		if (CrossPlatformInputManager.GetButton ("Jump")) {
			if (fuel > 0) { // fly
				myBody.velocity = new Vector2 (myBody.velocity.x, fly);
				// remove fuel
				levelManager.UpdateFuel (fly/1000 * -1);
			} else if (grounded > 0 || ladder > 0) { //jump
				myBody.velocity = new Vector2 (myBody.velocity.x, jump);
			}
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (enemyTag == other.gameObject.tag) {
			//dying
			levelManager.UpdateLives (-1);
			// active the effect
			Instantiate(explosion, this.transform.position, Quaternion.identity);
			//self-destroy
			Destroy (gameObject);

			levelManager.RestartLevel ();
		}

		if (((1 << other.gameObject.layer) & ladderLayer) != 0) {
			ladder++;
			myBody.velocity = new Vector2 (0, 0);
		}

		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			grounded++;
	}

	void OnTriggerExit2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & ladderLayer) != 0) {
			ladder--;
		}
		
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			grounded--;
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (enemyTag == col.collider.tag) {
			//dying
			levelManager.UpdateLives (-1);
			// active the effect
			Instantiate(explosion, this.transform.position, Quaternion.identity);
			//self-destroy
			Destroy (gameObject);

			levelManager.RestartLevel ();
		}
	}
}
