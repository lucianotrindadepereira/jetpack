﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicGravity : MonoBehaviour {

	public bool useAcceleration = true;

	float gravity = 0.0f;
	float deviceX = 0.0f;

	void Start() {
		gravity = Physics2D.gravity.y;//Default = -9.8
	}

	// Update is called once per frame
	void Update () {
		if (useAcceleration) {
			deviceX = Input.acceleration.x;
			//Debug.Log (deviceX + " " + deviceY);
			Physics2D.gravity = new Vector2 (deviceX * gravity * -1, Physics2D.gravity.y);
		}
	}
}
