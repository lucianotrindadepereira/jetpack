﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EditModeGridSnap : MonoBehaviour {

	public float snapValue = 1;

	Transform[] children;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float snapInverse =  1 / snapValue;
		float x, y;

		if (!Application.isPlaying) {
			children = GetComponentsInChildren<Transform> ();
			foreach (Transform child in children) {
				// only aplly changes to non-default layer
				if (child.gameObject.layer > 0) {
					x = Mathf.Round (child.gameObject.transform.position.x * snapInverse) / snapInverse;
					y = Mathf.Round (child.gameObject.transform.position.y * snapInverse) / snapInverse;
					child.gameObject.transform.position = new Vector3 (x, y, child.gameObject.transform.position.z);
				}
			}
		}
	}
}
