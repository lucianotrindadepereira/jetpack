﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontalCollider : MonoBehaviour {

	public LayerMask obstacleLayer;

	Transform parent;

	void Start() {
		// get parent transformation
		parent = this.transform.parent.GetComponent<Transform>();
	}

	void OnTriggerEnter2D (Collider2D other) {
		// flip
		if (((1 << other.gameObject.layer) & obstacleLayer) != 0)
			parent.localScale = new Vector3 (parent.localScale.x * -1, parent.localScale.y, parent.localScale.z);
	}
}
