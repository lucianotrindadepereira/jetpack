﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderColliderPosition : MonoBehaviour {

	LadderCollider ladderCollider;
	LayerMask ladderLayer;

	void Start () {
		ladderCollider = this.transform.parent.GetComponent<LadderCollider>();
		ladderLayer = ladderCollider.ladderLayer;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & ladderLayer) != 0)
			ladderCollider.setLadder (1);
	}

	void OnTriggerExit2D (Collider2D other) {
		if (((1 << other.gameObject.layer) & ladderLayer) != 0)
			ladderCollider.setLadder (-1);
	}
}
