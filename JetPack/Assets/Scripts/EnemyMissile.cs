﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMissile : MonoBehaviour {

	public int speed;
	public LayerMask obstacleLayer;
	public int artefactsLayer;
	public int ladderLayer;

	Rigidbody2D myBody;
	LevelManager levelManager;
	int sideAngle = 0;

	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();	
		myBody = this.GetComponent<Rigidbody2D> ();

		Physics2D.IgnoreLayerCollision (gameObject.layer, artefactsLayer);
		Physics2D.IgnoreLayerCollision (gameObject.layer, ladderLayer);
		Physics2D.IgnoreLayerCollision (gameObject.layer, gameObject.layer);
	}
	
	// Update is called once per frame
	void Update () {
		//movement
		if (levelManager.Freeze()) {
			myBody.velocity = new Vector2 (0, 0);
		} else {
			switch (sideAngle) {
			case 0:
				myBody.velocity = new Vector2 (speed, 0);
				break;
			case 90:
				myBody.velocity = new Vector2 (0, speed);
				break;
			case 180:
				myBody.velocity = new Vector2 (speed *-1, 0);
				break;
			case 270:
				myBody.velocity = new Vector2 (0, speed *-1);
				break;
			}
		}
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (((1 << col.gameObject.layer) & obstacleLayer) != 0) {
			if (Random.Range (-5, 10) > 0)
				sideAngle = sideAngle + 90;
			else
				sideAngle = sideAngle + 180;
			
			if (sideAngle >= 360)
				sideAngle = 0;
			else if (sideAngle < 0)
				sideAngle = 270;
			
			transform.localRotation = Quaternion.Euler (0, 0, sideAngle);
		}
	}
}
