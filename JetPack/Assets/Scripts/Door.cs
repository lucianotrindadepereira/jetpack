﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public GameObject doorOpened;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OpenDoor () {
		// open the door
		Instantiate (doorOpened, this.transform.position, Quaternion.identity);
		//self-destroy
		Destroy (gameObject);
	}
}
